# Build CaseFox Image
# `docker build . -t casefox`
# Run image
# `docker run -e DISCORD_TOKEN=FOO --restart=always casefox:latest`

FROM python:3.8

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY main.py .

ENV DISCORD_TOKEN=""
ENV PREFIX="/"
ENV NAME="CaseFox 📦🦊🤖"
ENV CASE_REVIEWER_ROLE="Mod"
ENV CASE_COMMAND_CHANNEL="general"
ENV CASE_CATEGORY="CASES"
ENV CASE_SUMMARY_CATEGORY="cases"
ENV CASE_SUMMARY_CHANNEL="case-summaries"

CMD echo "{ \"prefix\": \"$PREFIX\", \
  \"name\": \"$NAME\", \
  \"case_reviewer_role\": \"$CASE_REVIEWER_ROLE\", \
  \"key\":  \"$DISCORD_TOKEN\", \
  \"case_command_channel\": \"$CASE_COMMAND_CHANNEL\", \
  \"case_category\": \"$CASE_CATEGORY\", \
  \"summary_category\": \"$CASE_SUMMARY_CATEGORY\", \
  \"summary_channel\": \"$CASE_SUMMARY_CHANNEL\" \
}" > config.json && python main.py
