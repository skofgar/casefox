# CaseFox 📦🦊🤖

CaseFox is a Discord Bot that allows members to create *cases* that moderators will have access to and can discuss with the member. In the end the member or the mods may close the issue and it will be archived.

## Installation

Either install dependencies for this bot using [Conda](#conda) or [Python 3.8](#python)

### Conda

```bash
conda env create -f environment.yml
conda activate casefox
```

### Python

This bot is developed and tested on Python 3.8, though it might work on older versions.

```bash
pip install -r requirements.txt
```

## Create Bot and add it to your Discord server

1. Go to https://discord.com/developers/applications
2. Select "New Application"
3. Go to "Bot" and select "Add Bot"
4. Unselect "Public Bot" (this bot currently does not support being attached to multiple servers)
5. Copy the **TOKEN** and paste it into the `key` field in the `config.template.json`. Rename the file to `config.json`.
6. Go to OAuth and select `bot` and configure following permissions:
   - The bot needs permissions to _Manage Server, Manage Roles, Manage Channels, View Channels, Send Messages, Manage Messages, Embed Links and Read Message History_ (85008)
7. Open authorization url in the web browser and add the bot to your server.


## Discord Setup

1. Create a category for cases, e.g. 'CASES'
2. Grant the `CaseFox` role `Manage Channel, Manage Permissions and Read TextChannels` permissions
3. Create a `case-summaries` channel inside the `CASES` category and grant the `CaseFox` role `read messages, send messages` permissions
4. Grant the `CaseFox` role `Manage Messages` permissions in your `CASE_COMMAND_CHANNEL`
5. Make sure to grant your moderators, or whoever the _case reviewers_ are, read and write permissions to the `CASES` category, the `case-summaries` channel and set the name of that role in the `config.json` as `case_reviewer_role`.

## Run Bot

To run the bot, run:

```
python main.py
```

## Credit

This bot is inspired by the [Auroris Ticketing Bot](https://github.com/ifisq/discord-ticket-system)
