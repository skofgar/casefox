import json
import traceback

import discord
from discord.ext import commands
from discord.ext.commands import has_permissions

###################
# CONFIGURATION

with open("config.json") as f:
    config = json.load(f)
    CASE_COMMAND_CHANNEL = config["case_command_channel"]
    CASE_CATEGORY = config["case_category"]
    SUMMARY_CATEGORY = config["summary_category"]
    SUMMARY_CHANNEL = config["summary_channel"]
    PREFIX = config["prefix"]
    SYSTEM_NAME = config["name"]
    CASE_REVIEWER_ROLE = config["case_reviewer_role"]

client = commands.Bot(command_prefix=PREFIX)

CASE_COMMAND = PREFIX+'case'
DEFAULT_EMBED_COLOR = 0x00a8ff


###################
# COMMANDS

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))
    await client.change_presence(activity=discord.Game(name='Type '+ PREFIX +'case in '+ CASE_COMMAND_CHANNEL))


@client.command()
async def info(ctx):
    em = discord.Embed(title="About Me", description="Hello, my name is "+ SYSTEM_NAME +"! Use `" + CASE_COMMAND + "` to provide feedback, raise concerns or get help from mods. This command only works in the `#" + CASE_COMMAND_CHANNEL +"`. For more information type `" + PREFIX + "help`", color=DEFAULT_EMBED_COLOR)
    await ctx.send(embed=em)


@client.command(pass_context=True)
async def case(ctx):
    if not ctx.channel.name == CASE_COMMAND_CHANNEL:
        return

    roles = ctx.author.guild.roles
    moderator_role = next((x for x in roles if CASE_REVIEWER_ROLE in x.name), None)
    categories = ctx.author.guild.categories
    cases_category = next((x for x in categories if CASE_CATEGORY in x.name), None)

    bot_permissions = {
        ctx.guild.default_role: discord.PermissionOverwrite(read_messages=False),
        moderator_role: discord.PermissionOverwrite(view_channel=True, read_messages=True, read_message_history=True, send_messages=True, manage_channels=True),
        ctx.guild.me: discord.PermissionOverwrite(view_channel=True, read_messages=True, read_message_history=True, send_messages=True, manage_channels=True)
    }

    try:
        case_channel = await ctx.guild.create_text_channel("case_by_{}".format(ctx.author.name), overwrites=bot_permissions, category=cases_category)
    except Exception as e:
        em = discord.Embed(
                            title="Error"
                          , description="Couldn't create a case in the category '" + str(cases_category) + "'. Please contact an administrator."
                          , color=DEFAULT_EMBED_COLOR
                          )
        print("Exception name " + str(e))
        print("Permissions " + str(bot_permissions))
        print("Author " + str(ctx.author))
        traceback.print_exc()
        await ctx.send(embed=em)
        return

    try:
        await case_channel.set_permissions(ctx.author, send_messages=True, read_messages=True)
    except Exception as e:
        em = discord.Embed(
                            title="Error"
                          , description="Couldn't set permissions for case channel '" + str(case_channel) + "'. Please contact an administrator."
                          , color=DEFAULT_EMBED_COLOR
                          )
        print("Exception name " + str(e))
        print("Author " + str(ctx.author))
        traceback.print_exc()
        await ctx.send(embed=em)
        return
    #await case_channel.set_permissions(ctx.me, send_messages=True, read_messages=True)

    await case_channel.send('Hello {0}, \nPlease describe your case here. Please include details on:\n 1) why you are opening a case\n 2) include details, message links etc\n\nA mod will review it shortly.\nTo close the case type `/close`\n'.format(ctx.author.mention))

    msg = "Opening a new case, please hold..."

    await ctx.send(msg)
    await ctx.channel.purge(limit=2)


@client.command()
async def close(ctx):
    if ctx.channel.name == SUMMARY_CHANNEL:
        return

    def check(message):
        return message.author == ctx.author and message.channel == ctx.channel and message.content.lower() == "close"

    if ctx.channel.category.name == CASE_CATEGORY:
        em = discord.Embed(title="Close Case?", description="Are you sure you want to close this case? Reply with `close` if you are sure.", color=DEFAULT_EMBED_COLOR)
        await ctx.send(embed=em)
        await client.wait_for('message', check=check, timeout=60)
        await saveCaseHistory(ctx)
        await ctx.channel.delete()


async def saveCaseHistory(ctx):
    categories = ctx.author.guild.categories
    cases_category = next((x for x in categories if x.name == SUMMARY_CATEGORY), None)
    summary_channel = next((x for x in cases_category.channels if x.name == SUMMARY_CHANNEL), None)

    # await cases_category.set_permissions(ctx.me, view_channel=True, manage_channels=True)
    # await summary_channel.set_permissions(ctx.me, read_messages=True, send_messages=True)

    em = await getHistoryEmbed(ctx)
    await summary_channel.send(embed=em)


async def getHistoryEmbed(ctx):
    historyString = await getHistoryString(ctx)
    channelName = str(ctx.channel.name)
    em = discord.Embed(title="Case History: " + channelName, description="This is the summary of the case `{}`\n\n\n{}".format(channelName, historyString), color=DEFAULT_EMBED_COLOR)
    em.set_footer(text="by "+SYSTEM_NAME)
    return em


async def getHistoryString(ctx):
    def predicate(message):
        return True  # filter messages if needed, e.g. `not message.author.bot`

    messages = ctx.channel.history().filter(predicate)

    messageHistory = []
    async for message in messages:
        messageHistory.insert(0, message)

    historyString = ' '.join([str(str(elem.author) + ': ' + elem.content + '\n') for elem in messageHistory])
    return historyString


@client.command()
@has_permissions(administrator=True)
async def changestatus(ctx, *args):

    previousPresence = str(ctx.guild.me.activity)

    await client.change_presence(activity=discord.Game(name=' '.join(args)))

    newPresence = str(ctx.guild.me.activity)

    embed=discord.Embed(title="Status changed!", description="The bot's status has been successfully updated from '" + previousPresence + "' to '" + newPresence  +  "' by " + str(ctx.author.name) + ".", color=DEFAULT_EMBED_COLOR)
    embed.set_footer(text="by " + SYSTEM_NAME)
    await ctx.send(embed=embed)


@client.command()
async def ping(ctx):
    embed = discord.Embed(title="Pong! 🏓", description="", color=DEFAULT_EMBED_COLOR)
    embed.set_footer(text=f"{round(client.latency * 1000)}ms")
    await ctx.send(embed=embed)


@client.remove_command("help")
@client.command(name="help", description="Returns all commands available")
async def help(ctx):
    embed = discord.Embed(title="Help", description="**Commands:**", color=DEFAULT_EMBED_COLOR)
    embed.add_field(name= PREFIX + "case", value = "Creates an new channel in the " + CASE_CATEGORY + " category, in which you can describe your case", inline = False)
    embed.add_field(name= PREFIX + "close", value = "Closes the channel in the " + CASE_CATEGORY + " category", inline=False)
    embed.add_field(name=PREFIX + "ping", value="Sends a ping to the bot and returns the latency in milliseconds.", inline=False)
    if ctx.author.guild_permissions.administrator:
        embed.add_field(name= PREFIX + "changestatus", value = "Changes the bots Status", inline = False)
    embed.set_footer(text="Please note that the command to open a new case only works in the " + CASE_COMMAND_CHANNEL + " channel.")
    await ctx.send(embed=embed)


###################
# STARTUP

client.run(config["key"])
